package ru.kampaii;

import static org.junit.Assert.*;

/**
 * Created by Kirill on 11.05.2019.
 */
public class CacheExampleTest {
    @org.junit.Test
    public void testLRUStorage() throws Exception {
        LRUStorage storage = new LRUStorage(3);
        CacheExample ce = new CacheExample(storage);

        ce.put("1","test1");
        ce.put("2","test2");
        ce.put("3","test3");
        ce.printState();
        // adding new object to the top, the tail key = 1 has to be deleted
        ce.put("4","test4");
        ce.printState();
        assertNull(ce.get("1"));
    }

    @org.junit.Test
    public void testLFUStorage() throws Exception {
        LFUStorage storage = new LFUStorage(3);
        CacheExample ce = new CacheExample(storage);

        ce.put("1","test1");
        ce.put("2","test2");
        ce.put("3","test3");
        ce.get("1");
        ce.get("1");
        ce.get("1");
        ce.get("2");
        ce.get("2");
        ce.printState();
        // adding new object, the least frequently used key = "3" has to be deleted
        ce.put("4","test4");
        ce.printState();

        assertNull(ce.get("3"));
    }
}