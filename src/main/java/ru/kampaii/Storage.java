package ru.kampaii;

import java.util.HashMap;

public interface Storage<K, T> {

    public void put(K key, T obj);

    public T get(K key);

    public void remove(K key);

    public int size();

    public void printCurrentState();

    public HashMap<K,T> getAll();
}
