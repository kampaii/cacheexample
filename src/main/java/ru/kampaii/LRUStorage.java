package ru.kampaii;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Kirill on 11.05.2019.
 */
public class LRUStorage<K, T> implements Storage {

    private LinkedHashMap storage;

    public LRUStorage(final int size) {
        this.storage = new LinkedHashMap(size){
            @Override
            protected boolean removeEldestEntry(Map.Entry eldest){
                if (size() > size) {
                    System.out.println("LRUStorage.removeEldestEntry");
                    return true;
                }
                return false;
            }
        };
        System.out.println("LRU storage initialized");
    }

    public void put(Object key, Object obj) {
        this.storage.put(key,obj);
    }

    public Object get(Object key) {
        return this.storage.get(key);
    }

    public void remove(Object key) {
        this.storage.remove(key);
    }

    public int size() {
        return this.storage.size();
    }

    public void printCurrentState() {
        System.out.println("LRUStorage : {");
        for (Object obj: this.storage.values()) {
            System.out.println("    "+obj.toString());
        }
        System.out.println("}");
    }

    public HashMap getAll() {
        return this.storage;
    }
}
