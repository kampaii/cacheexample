package ru.kampaii;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Kirill on 11.05.2019.
 */
public class LFUStorage<K, T> implements Storage {

    private LinkedHashMap<K, StoredObject> storage;
    private int capacity;

    protected class StoredObject{
        private T obj;
        private int frequency;

        public StoredObject(T obj){
            this.obj = obj;
            this.frequency = 1;
        }

        public int getFrequency(){
            return this.frequency;
        }

        public void incFrequency(){
            this.frequency = this.frequency + 1;
        }

        public T getObj() {
            return obj;
        }
    }

    public LFUStorage(final int size) {
        this.storage = new LinkedHashMap(size);
        this.capacity = size;
        System.out.println("LRU storage initialized");
    }

    private K getLFUkey(){
        int min = Integer.MAX_VALUE;
        K res = null;

        for( Map.Entry<K,StoredObject> entry : this.storage.entrySet()){
            int fr = entry.getValue().getFrequency();
            if (min > fr){
                min = fr;
                res = entry.getKey();
            }
        }
        // return null will never happen because we are calling this on an non-empty collection
        return res;
    }

    public void put(Object key, Object obj) {
        // if there is a place for new object, just simply put
        if(storage.size() < capacity){
            storage.put((K) key,new StoredObject((T) obj));
        }
        else {
            storage.remove(getLFUkey());
            storage.put((K) key,new StoredObject((T) obj));
        }
    }

    public Object get(Object key) {
        StoredObject so = this.storage.get(key);
        if(so != null){
            so.incFrequency();
        }
        else{
            return null;
        }
        return so.obj;
    }

    public void remove(Object key) {
        this.storage.remove(key);
    }

    public int size() {
        return this.storage.size();
    }

    public void printCurrentState() {
        System.out.println("LFUStorage : {");
        for (StoredObject obj: this.storage.values()) {
            System.out.println("    obj : "+obj.obj.toString()+", frequency: "+obj.getFrequency());
        }
        System.out.println("}");
    }

    public HashMap getAll() {
        return this.storage;
    }
}
