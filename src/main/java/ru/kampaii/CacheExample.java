package ru.kampaii;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Kirill on 11.05.2019.
 */
public class CacheExample<K, T> {

    private Storage storage;

    public CacheExample(Storage storage){
        this.storage = storage;
    }

    public CacheExample(String type, int size){
        if (type == "LRU"){
            this.storage = new LRUStorage<K,T>(size);
        }
        else {
            this.storage = new LFUStorage<K,T>(size);
        }
    }

    public void put(K key,T obj){
        storage.put(key,obj);
    }

    public T get(K key){
        return (T)storage.get(key);
    }

    public void remove(K key){
        storage.remove(key);
    }

    public int size(){
        return storage.size();
    }

    public void printState(){
        this.storage.printCurrentState();
    }

    public void reinit(Storage newStorage){
        this.storage = newStorage;
    }
}
